from __future__ import print_function
import sys
import cv2
from random import randint
  
videoPath = "test2.mp4"
cap = cv2.VideoCapture(0)

success, frame = cap.read()

if not success:
  print('Failed to read video')
  sys.exit(1)

bboxes = []
colors = [] 

while True:
  bbox = cv2.selectROI('MultiTracker', frame)
  bboxes.append(bbox)
  colors.append((randint(0, 255), randint(0, 255), randint(0, 255)))
  print("Press q to quit selecting boxes and start tracking")
  print("Press any other key to select next object")
  k = cv2.waitKey(0) & 0xFF
  if (k == 113):  # q is pressed
    break

print('Selected bounding boxes {}'.format(bboxes))

trackerType = "CSRT"    
multiTracker = cv2.MultiTracker_create()

for bbox in bboxes:
  multiTracker.add(cv2.TrackerCSRT_create(), frame, bbox)

while cap.isOpened():
  success, frame = cap.read()
  success, boxes = multiTracker.update(frame)

  print (str(len(bboxes)) + ' before ' + str(len(boxes)))
  isBoxOut = False
    
  for i, newbox in enumerate(boxes):
    p1 = (int(newbox[0]), int(newbox[1]))
    p2 = (int(newbox[0] + newbox[2]), int(newbox[1] + newbox[3]))
    cv2.rectangle(frame, p1, p2, colors[i], 2, 1)
    if p1[0] <=10 or p1[1] <=10:
        bboxes.pop(i)
        isBoxOut = True

  if isBoxOut:
    multiTracker.clear()
    multiTracker = cv2.MultiTracker_create()
    for bbox in bboxes:
      multiTracker.add(createTrackerByName(trackerType), frame, bbox)
	  
  print (str(len(bboxes)) + ' after ' + str(len(boxes)))
  cv2.imshow('MultiTracker', frame)
  
  if cv2.waitKey(1) & 0xFF == 27:  # Esc pressed
    break